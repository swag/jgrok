import ca.uwaterloo.cs.jgrok.env.Env;
import ca.uwaterloo.cs.jgrok.interp.Interp;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

public class RegressionTest {

    private static InputStream nullInputStream = new InputStream() {
        @Override
        public int read() throws IOException {
            return -1;
        }
    };

    @TestFactory
    public Stream<DynamicTest> makeRegressionTests(){
        return Stream.of(
            Arrays.asList("globalVarMissingArg", "globalVar.ql"),
            Arrays.asList("globalVarSandwichBarBFX", "globalVar.ql", "test/regression_data/SandwichBar.bfx.ta")
            //Arrays.asList("globalVarBig", "extractHadoop.ql")
        ).map(params -> dynamicTest(params.get(0), () -> {

            String testName = params.get(0);
            String script = params.get(1);
            String[] scriptArgs = new String[params.size() - 1];
            params.subList(1, params.size()).toArray(scriptArgs);

            List<String> outExpected = Files
                    .lines(Paths.get("test", "regression_data", testName+".outexpected"))
                    .collect(Collectors.toList());
            List<String> errExpected = Files
                    .lines(Paths.get("test", "regression_data", testName+".errexpected"))
                    .collect(Collectors.toList());

            Env testEnv = new Env();
            try(ByteArrayOutputStream outArray = new ByteArrayOutputStream();
                ByteArrayOutputStream errArray = new ByteArrayOutputStream();
                PrintStream outStream = new PrintStream(outArray);
                PrintStream errStream = new PrintStream(errArray)) {

                testEnv.out = outStream;
                testEnv.err = errStream;
                testEnv.in = nullInputStream;

                Interp interp = Interp.reinit(Paths.get("test", "regression_data", script).toFile());
                interp.bootstrapEvaluate(testEnv, scriptArgs);

                assertAll(
                        () -> assertLinesMatch(
                                outExpected,
                                Arrays.asList(outArray.toString().split(System.lineSeparator()))),
                        () -> assertLinesMatch(
                                errExpected,
                                Arrays.asList(errArray.toString().split(System.lineSeparator())))
                );
            }
        }));
    }
}
